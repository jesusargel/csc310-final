import java.nio.*;
import java.lang.Math;
import javax.swing.*;

import static com.jogamp.opengl.GL.GL_ARRAY_BUFFER;
import static com.jogamp.opengl.GL.GL_DEPTH_TEST;
import static com.jogamp.opengl.GL.GL_FLOAT;
import static com.jogamp.opengl.GL.GL_STATIC_DRAW;
import static com.jogamp.opengl.GL.GL_TEXTURE0;
import static com.jogamp.opengl.GL.GL_TEXTURE_2D;
import static com.jogamp.opengl.GL.GL_TRIANGLES;
import static com.jogamp.opengl.GL4.*;
import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.*;

import graphicslib3D.Vertex3D;

import com.jogamp.common.nio.Buffers;
import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;

import org.joml.*;

public class Code extends JFrame implements GLEventListener, java.awt.event.KeyListener
{	
	private GLCanvas myCanvas;
	private double startTime = 0.0;
	private double elapsedTime;
	private int renderingProgram;
	private int vao[] = new int[2];
	private int vbo[] = new int[5];
	private float cameraX, cameraY, cameraZ;


	//allocate variables for display() function
	private FloatBuffer vals = Buffers.newDirectFloatBuffer(16);
	private Matrix4fStack mvStack = new Matrix4fStack(10);
	private Matrix4f pMat = new Matrix4f();
	private Matrix4f vMat = new Matrix4f();
	private int mvLoc, projLoc, nLoc;
	private float aspect;
	private double tf;
	
	//sphere
	private Sphere mySphere;

	//textures
	private int basketBallTexture;
	private int soccerBallTexture;
	private int baseballTexture;

	//object
	private int numObjVertices;
	private ImportedModel myModel;

	public Code()
	{	
	setTitle("CSCI-310 FINAL PROJECT");
	setSize(800, 800);
	myCanvas = new GLCanvas();
	mySphere = new Sphere(48);
	myCanvas.addGLEventListener(this);
	myCanvas.addKeyListener(this);
	
	this.add(myCanvas);
	this.setVisible(true);
	Animator animator = new Animator(myCanvas);
	animator.start();
	}

	public void display(GLAutoDrawable drawable)
	{	
		GL4 gl = (GL4) GLContext.getCurrentGL();
		gl.glClear(GL_COLOR_BUFFER_BIT);
		gl.glClear(GL_DEPTH_BUFFER_BIT);
		elapsedTime = System.currentTimeMillis() - startTime;

		gl.glUseProgram(renderingProgram);

		mvLoc = gl.glGetUniformLocation(renderingProgram, "mv_matrix");
		projLoc = gl.glGetUniformLocation(renderingProgram, "proj_matrix");

		aspect = (float) myCanvas.getWidth() / (float) myCanvas.getHeight();
		pMat.identity().setPerspective((float) Math.toRadians(60.0f), aspect, 0.1f, 1000.0f);
		gl.glUniformMatrix4fv(projLoc, 1, false, pMat.get(vals));
	
	// push view matrix onto the stack
	mvStack.pushMatrix();
	mvStack.translate(-cameraX, -cameraY, -cameraZ);

	tf = elapsedTime/1000.0;  // time factor

	// ---------------------- soccer ball  
	mvStack.pushMatrix();
	mvStack.translate(1.0f, 0.0f, 0.0f);
	mvStack.pushMatrix();
	mvStack.rotate((float)tf, 1.0f, 0.0f, 0.0f);
	mvStack.scale(0.50f, 0.50f, 0.50f);
	gl.glUniformMatrix4fv(mvLoc, 1, false, mvStack.get(vals));
	gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	gl.glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
	gl.glEnableVertexAttribArray(0);
	gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	gl.glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
	gl.glEnableVertexAttribArray(1);
	gl.glActiveTexture(GL_TEXTURE0);
	gl.glBindTexture(GL_TEXTURE_2D, soccerBallTexture);
	gl.glEnable(GL_DEPTH_TEST);
	int numVerts = mySphere.getIndices().length;
	gl.glDrawArrays(GL_TRIANGLES, 0, numVerts);
	mvStack.popMatrix();
	mvStack.popMatrix();


	//---------------------- basketball
	
	mvStack.pushMatrix();
	mvStack.translate(-2.0f, (float)Math.cos(tf)*2.0f + 2.3f, 0.0f);
	mvStack.pushMatrix();
	mvStack.rotate((float)tf*2, 1.0f, 0.0f, 0.0f);
	mvStack.scale(0.75f, -0.75f, -0.75f);
	gl.glUniformMatrix4fv(mvLoc, 1, false, mvStack.get(vals));
	gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	gl.glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
	gl.glEnableVertexAttribArray(0);
	gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	gl.glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
	gl.glEnableVertexAttribArray(1);
	gl.glActiveTexture(GL_TEXTURE0);
	gl.glBindTexture(GL_TEXTURE_2D, basketBallTexture);
	gl.glEnable(GL_DEPTH_TEST);
	gl.glDrawArrays(GL_TRIANGLES, 0, numVerts);
	mvStack.popMatrix();
	mvStack.popMatrix();
	
	
	//------------------------ baseball
	
	mvStack.pushMatrix();
	mvStack.translate(2.5f, 1.3f, 0.0f);
	mvStack.pushMatrix();
	mvStack.rotate((float)tf*4, 1.0f, 0.0f, 0.0f);
	mvStack.scale(0.2f, 0.2f, 0.2f);
	gl.glUniformMatrix4fv(mvLoc, 1, false, mvStack.get(vals));
	gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
	gl.glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
	gl.glEnableVertexAttribArray(0);
	gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
	gl.glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
	gl.glEnableVertexAttribArray(1);
	gl.glActiveTexture(GL_TEXTURE0);
	gl.glBindTexture(GL_TEXTURE_2D, baseballTexture);
	gl.glEnable(GL_DEPTH_TEST);
	gl.glDrawArrays(GL_TRIANGLES, 0, myModel.getNumVertices());
	mvStack.popMatrix();
	mvStack.popMatrix();
	

	mvStack.popMatrix();

	}

	public void init(GLAutoDrawable drawable)
	{	GL4 gl = (GL4) GLContext.getCurrentGL();
	myModel = new ImportedModel("baseball.obj");
	startTime = System.currentTimeMillis();
	renderingProgram = Utils.createShaderProgram("src/vert.shader", "src/frag.shader");
	setupVertices();
	
	cameraX = 0.0f; cameraY = 0.0f; cameraZ = 12.0f;

	basketBallTexture = Utils.loadTexture("src/basketball.jpg");
	soccerBallTexture = Utils.loadTexture("src/soccerball.jpg");
	baseballTexture = Utils.loadTexture("src/baseball.jpg");
	}

	
	private void setupVertices()
	{	
		GL4 gl = (GL4) GLContext.getCurrentGL();

		Vertex3D[ ] vertices = mySphere.getVertices();
		int[ ] indices = mySphere.getIndices();

		float[ ] pvalues = new float[indices.length*3]; // vertex positions
		float[ ] tvalues = new float[indices.length*2]; // texture coordinates

		for (int i=0; i<indices.length; i++)
		{ pvalues[i*3] = (float) (vertices[indices[i]]).getX();
		pvalues[i*3+1] = (float) (vertices[indices[i]]).getY();
		pvalues[i*3+2] = (float) (vertices[indices[i]]).getZ();
		tvalues[i*2] = (float) (vertices[indices[i]]).getS();
		tvalues[i*2+1] = (float) (vertices[indices[i]]).getT();
		}
		
		numObjVertices = myModel.getNumVertices();
		Vector3f[] vertices1 = myModel.getVertices();
		Vector2f[] texCoords1 = myModel.getTexCoords();
		Vector3f[] normals1 = myModel.getNormals();
		
		float[] pvalues1 = new float[numObjVertices*3];
		float[] tvalues1 = new float[numObjVertices*2];
		float[] nvalues1 = new float[numObjVertices*3];
		
		for (int i=0; i<numObjVertices; i++)
		{	pvalues1[i*3]   = (float) (vertices1[i]).x();
			pvalues1[i*3+1] = (float) (vertices1[i]).y();
			pvalues1[i*3+2] = (float) (vertices1[i]).z();
			tvalues1[i*2]   = (float) (texCoords1[i]).x();
			tvalues1[i*2+1] = (float) (texCoords1[i]).y();
			nvalues1[i*3]   = (float) (normals1[i]).x();
			nvalues1[i*3+1] = (float) (normals1[i]).y();
			nvalues1[i*3+2] = (float) (normals1[i]).z();
		}

		gl.glGenVertexArrays(vao.length, vao, 0);
		gl.glBindVertexArray(vao[0]);
		gl.glGenBuffers(vbo.length, vbo, 0);

		// put the vertices into buffer #0
		gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		FloatBuffer vertBuf = Buffers.newDirectFloatBuffer(pvalues);
		gl.glBufferData(GL_ARRAY_BUFFER, vertBuf.limit()*4, vertBuf, GL_STATIC_DRAW);

		// put the texture coordinates into buffer #1
		gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
		FloatBuffer texBuf = Buffers.newDirectFloatBuffer(tvalues);
		gl.glBufferData(GL_ARRAY_BUFFER, texBuf.limit()*4, texBuf, GL_STATIC_DRAW);
		
		
		gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
		FloatBuffer vertBuf1 = Buffers.newDirectFloatBuffer(pvalues1);
		gl.glBufferData(GL_ARRAY_BUFFER, vertBuf1.limit()*4, vertBuf1, GL_STATIC_DRAW);

		gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
		FloatBuffer texBuf1 = Buffers.newDirectFloatBuffer(tvalues1);
		gl.glBufferData(GL_ARRAY_BUFFER, texBuf1.limit()*4, texBuf1, GL_STATIC_DRAW);

		gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[4]);
		FloatBuffer norBuf1 = Buffers.newDirectFloatBuffer(nvalues1);
		gl.glBufferData(GL_ARRAY_BUFFER, norBuf1.limit()*4,norBuf1, GL_STATIC_DRAW);

	}

	public static void main(String[] args) { new Code(); }
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {}
	public void dispose(GLAutoDrawable drawable) {}

		


	@Override
	public void keyPressed(java.awt.event.KeyEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getKeyChar() == 'r')
		{
			cameraX = 0.0f; cameraY = 0.0f; cameraZ = 12.0f;
		}
		if(arg0.getKeyChar() == 'a')
		{
			cameraX = 1.0f; cameraY = 0.0f; cameraZ = 2.0f;
		}
		if(arg0.getKeyChar() == 's')
		{
			cameraX = -2.0f; cameraY = 0.0f; cameraZ = 3.0f;
		}
		if(arg0.getKeyChar() == 'd')
		{
			cameraX = 2.5f; cameraY = 1.6f; cameraZ = 1.5f;
		}		
	}

	@Override
	public void keyReleased(java.awt.event.KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(java.awt.event.KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}